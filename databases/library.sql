DROP DATABASE IF EXISTS library;
CREATE DATABASE library;
USE library;

CREATE TABLE users(
    id INTEGER AUTO_INCREMENT,
    name VARCHAR(20) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE books(
    id INTEGER AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    author VARCHAR(20) NOT NULL,
    publish_year DATE NOT NULL,
    editor VARCHAR(20) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE loans(
    id INTEGER AUTO_INCREMENT,
    id_user INTEGER,
    id_book INTEGER,
    requested BOOLEAN NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(id_user) REFERENCES users(id),
    FOREIGN KEY(id_book) REFERENCES books(id)
);

INSERT INTO users(name)
VALUES('Narayami'), 
('Ze'), 
('Jessy'), 
('alakazam');

INSERT INTO books(name, author, publish_year, editor)
VALUES ('Lord of the Rings', 'Tolkien', '1990-12-05', 'tokien_company'), 
('Matrix', 'Jiraya', '2000-03-06', 'matrix_company'), 
('Hookers Guide', 'sheila', '2018-01-02', 'hoe_bar'), 
('Trip to hell', 'hellkaiser', '2001-05-10', 'demon_lord');

INSERT INTO loans(id_user, id_book, requested)
VALUES (1, 1, true),
(2, 1, true),
(3, 3, false), 
(3, 4, true);

/*
SELECT u.name
FROM users AS u
WHERE u.id IN(
    SELECT l.id_user
    FROM loans AS l
    WHERE l.id_book IN(
        SELECT b.id
        FROM books AS b
        WHERE b.publish_year = '1990-12-05'
    )
);*/
