DROP DATABASE IF EXISTS summarizer;

CREATE DATABASE summarizer;
USE summarizer;

CREATE TABLE race(
    id INTEGER AUTO_INCREMENT,
    racename VARCHAR(20) NOT NULL,
    trait VARCHAR(20) NOT NULL,
    PRIMARY KEY(id)
);


INSERT INTO race(racename, trait)
VALUES ('human', 'adaptability'), ('elf', 'good eyesight');

CREATE TABLE skills(
    id INTEGER AUTO_INCREMENT,
);