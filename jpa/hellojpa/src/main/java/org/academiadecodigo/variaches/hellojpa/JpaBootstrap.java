package org.academiadecodigo.variaches.hellojpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaBootstrap {

    public static void main(String[] args) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("test");

        EntityManager manager = factory.createEntityManager();

        System.out.println(manager.createNativeQuery("SELECT username FROM user WHERE username = rui ")
                .getResultList().get(1));


        manager.close();
        factory.close();

    }

}
