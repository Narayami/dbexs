DROP DATABASE IF EXISTS mappings;
CREATE DATABASE mappings;
USE mappings;

-- mapped superclass
CREATE TABLE mapped_customer(
    id INTEGER,
    name VARCHAR(255),
    creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    update_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    version INTEGER,
    PRIMARY KEY(id)
);

CREATE TABLE mapped_account(
    id INTEGER,
    balance REAL,
    creation_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    update_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    version INTEGER,
    PRIMARY KEY (id)
);


-- table per class
CREATE TABLE table_boat(
    id INTEGER,
    max_speed INTEGER,
    engines INTEGER,
    PRIMARY KEY (id)
);

CREATE TABLE table_car(
    id INTEGER,
    max_speed INTEGER,
    gears INTEGER,
    PRIMARY KEY (id)
);


-- single table
CREATE TABLE single_vehicle(
    id INTEGER AUTO_INCREMENT,
    vehicle_type VARCHAR(255),
    max_speed INTEGER,
    gears INTEGER,
    engines INTEGER,
    PRIMARY KEY (id)
);

-- joined table
CREATE TABLE joined_vehicle(
    id INTEGER,
    max_speed INTEGER,
    PRIMARY KEY(id)
);

CREATE TABLE joined_boat(
    id INTEGER,
    engines INTEGER,
    PRIMARY KEY(id)
);

CREATE TABLE joined_car(
    id INTEGER,
    gears INTEGER,
    PRIMARY KEY(id)
);

-- one to one
CREATE TABLE one_one_owner(
    id INTEGER,
    name VARCHAR(255),
    PRIMARY KEY(id)
);

CREATE TABLE one_one_car(
    id INTEGER,
    make VARCHAR(255),
    model VARCHAR(255),
    owner_id INTEGER,
    PRIMARY KEY(id),
    FOREIGN KEY (owner_id) REFERENCES one_one_owner(id)
);

-- many to one
CREATE TABLE many_one_category(
    id INTEGER,
    name VARCHAR(255),
    PRIMARY KEY(id)
);

CREATE TABLE many_one_product(
    id INTEGER,
    name VARCHAR(255),
    category_id INTEGER,
    PRIMARY KEY(id),
    FOREIGN KEY(category_id) REFERENCES many_one_category(id)
);

-- many to many
CREATE TABLE many_many_user(
    id INTEGER,
    password VARCHAR(255),
    username VARCHAR(255),
    PRIMARY KEY(id)
);

CREATE TABLE many_many_group(
    id INTEGER, 
    name VARCHAR(255),
    PRIMARY KEY(id)
);

CREATE TABLE many_many_users_groups(
    user_id INTEGER,
    group_id INTEGER,
    PRIMARY KEY(user_id, group_id),
    FOREIGN KEY(user_id) REFERENCES many_many_user(id),
    FOREIGN KEY(group_id) REFERENCES many_many_group(id)
);
