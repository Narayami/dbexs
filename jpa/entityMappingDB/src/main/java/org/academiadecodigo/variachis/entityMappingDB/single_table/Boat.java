package org.academiadecodigo.variachis.entityMappingDB.single_table;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@DiscriminatorValue("boat")
@Table(name = "table_boat")
public class Boat extends Vehicle {
    private Integer engines;

}


