package org.academiadecodigo.variachis.entityMappingDB.single_table;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@DiscriminatorValue("car")
@Table(name = "table_car")
public class Car extends Vehicle {
    private Integer gears;

}
