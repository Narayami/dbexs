package org.academiadecodigo.variachis.entityMappingDB.single_table;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.RollbackException;

public class SingleVehicleManager {

    private EntityManagerFactory factory;


    public SingleVehicleManager(EntityManagerFactory factory){
        this.factory = factory;
    }

    public Vehicle findById(Integer id) {

        EntityManager manager = factory.createEntityManager();

        try {
            manager = factory.createEntityManager();

            return manager.find(Vehicle.class, id);

        } finally {
            if (manager != null) {
                manager.close();
            }
        }
    }

    public Vehicle updateSailor(String name){
        return null;
    }

    public void addVehicle(Vehicle vehicle){
        EntityManager manager = factory.createEntityManager();

        try {
            manager.getTransaction().begin();
            manager.persist(vehicle);
            manager.getTransaction().commit();
        }catch (RollbackException ex){
          manager.getTransaction().rollback();

        } finally {
            if (manager != null){
                manager.close();
            }
        }
    }
}
