package org.academiadecodigo.variachis.entityMappingDB;

import org.academiadecodigo.variachis.entityMappingDB.single_table.Vehicle;
import org.academiadecodigo.variachis.entityMappingDB.single_table.SingleVehicleManager;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

    public static void main(String[] args) {

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("test");
        SingleVehicleManager singleVehicleManager = new SingleVehicleManager(factory);

        //singleVehicleManager.addVehicle(new Vehicle(3, 5));

        //System.out.println(singleVehicleManager.findById(1).getGears());

    }



}
