package org.academiadecodigo.variachis.entityMappingDB.single_table;


import javax.persistence.*;

@Entity
@Table(name = "single_vehicle")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name = "vehicle_type",
        discriminatorType = DiscriminatorType.STRING
)
public abstract class Vehicle {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer max_speed;

}
